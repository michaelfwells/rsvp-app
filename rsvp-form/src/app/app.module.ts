import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { RSVPService } from './rsvp.service';
import { RSVPFormComponent } from './rsvpform/rsvpform.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RSVPFormComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [RSVPService],
  entryComponents: [RSVPFormComponent]
})
export class AppModule {
  constructor(injector: Injector) {

  const rsvpFormElement = createCustomElement(RSVPFormComponent, {injector});

    customElements.define('rsvp-form', rsvpFormElement)
  }

  ngDoBootstrap() {

  }
 }
