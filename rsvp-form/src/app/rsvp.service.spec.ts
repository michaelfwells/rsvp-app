import { TestBed } from '@angular/core/testing';

import { RSVPService } from './rsvp.service';

describe('RSVPService', () => {
  let service: RSVPService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RSVPService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
