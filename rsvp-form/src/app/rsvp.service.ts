import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GuestList, Guest } from './models/GuestList';

@Injectable({
  providedIn: 'any'
})
export class RSVPService {

  constructor(private http:HttpClient) {

   }

   submitRsvp(guestList:GuestList) {
      let guests:Guest[] = guestList.attending.concat(guestList.declining);
      let data = { data: guests};
      console.log(data);
      return this.http.post('https://sheetdb.io/api/v1/2c6eqob4q3yed', data);
   }
}
