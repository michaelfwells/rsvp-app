import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { RSVPService } from '../rsvp.service';
import { GuestList, Guest } from '../models/GuestList';
import { $ } from 'protractor';
isSubmitted: false;
@Component({
  templateUrl: './rsvpform.component.html',
  styleUrls: ['./rsvpform.component.scss']
})
export class RSVPFormComponent implements OnInit {
  attendingCount = 0;
  decliningCount = 0;
  public guestList:GuestList;
  minusIcon = faMinusCircle;
  plusIcon = faPlusCircle;
  test = "test";
  isSubmitted = false;

  constructor(private service:RSVPService) {

   }

   public submit() {
    this.service.submitRsvp(this.guestList).subscribe(result => {
      console.log(result);
      this.isSubmitted = true;
      });
  }


  ngOnInit(): void {
    this.guestList = new GuestList();
    console.log(this.guestList);
    console.log(this.guestList.attending);
    console.log(this.guestList.attending.length);
  }

}


