export class GuestList {
    public attending:Guest[];
    public declining:Guest[];
    constructor() {
      this.attending = [];
      this.declining = [];
    }
    
    public attendingCount() {
      console.log(this.attending);
      console.log(this.attending.length)
      return this.attending.length;
    }
  
    public decliningCount() {
      return this.declining.length
    }
  
    public addAttending() {
      this.attending.push(new Guest("yes"));
    }
  
    public addDeclining() {
      this.declining.push(new Guest("no"));
    }
  
    public removeAttending() {
      if (this.attendingCount() > 0) {
        this.attending.splice(this.attending.length - 1);
      }
    }
  
    public removeDeclining() {
      if (this.decliningCount() > 0) {
        this.declining.splice(this.declining.length - 1);
      }   
    }
  }
  
  export class Guest {
    public firstName:string;
    public lastName:string;
    public attending:string;
  
    constructor(isAttending:string) {
        this.firstName = "";
        this.lastName = "";
        this.attending = isAttending;
    }
  }